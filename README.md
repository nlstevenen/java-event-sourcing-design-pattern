# Event Sourcing Design Pattern #

#Implementation Details

To experiment with the event sourcing design pattern I've decided to build a sort of soccer match system. It can be used by executing commands which result in events. These events can be stored to restore a match. It's also possible to sequentially walk through the occured events and go back to a certain moment in time. 

Events are kept as small as possible and refer to a certain command. Commands hold the execution logic.