import match.aggregate.MatchState;
import match.command.AssignRedCardCommand;
import match.command.AssignYellowCardCommand;
import match.command.EndMatchCommand;
import match.command.StartMatchCommand;
import match.handler.MatchHandler;
import match.player.PlayerModel;
import match.store.MemoryEventStore;
import match.store.MemoryEventStream;
import testModels.SoccerMatchBuilder;
import testModels.SoccerMatchModel;
import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;


public class RestoringStateTest {

    private MemoryEventStream testMatchA() {

        SoccerMatchModel soccerMatch = SoccerMatchBuilder.build();
        UUID matchId = soccerMatch.getMatch().getMatchId();

        MemoryEventStore memoryEventStore = new MemoryEventStore();
        MatchHandler matchHandler = new MatchHandler(soccerMatch.getMatch(), memoryEventStore);

        // Start
        matchHandler.handle(new StartMatchCommand(matchId));


        // Give cards
        for (PlayerModel playerModel : soccerMatch.getPlayerSet()) {
            if (playerModel.getPlayerName().equals("frank")) {
                matchHandler.handle(new AssignYellowCardCommand(matchId, playerModel));
            }
            if (playerModel.getPlayerName().equals("piet")) { // assign two yellow cards
                matchHandler.handle(new AssignYellowCardCommand(matchId, playerModel));
                matchHandler.handle(new AssignYellowCardCommand(matchId, playerModel));
            }
            if (playerModel.getPlayerName().equals("henk")) {
                matchHandler.handle(new AssignRedCardCommand(matchId, playerModel));
            }

        }

        matchHandler.handle(new EndMatchCommand(matchId));
        return memoryEventStore.loadEventStream(matchId);
    }

    private MemoryEventStream testMatchB() {
        SoccerMatchModel soccerMatch = SoccerMatchBuilder.build();
        UUID matchId = soccerMatch.getMatch().getMatchId();

        MemoryEventStore memoryEventStore = new MemoryEventStore();
        MatchHandler matchHandler = new MatchHandler(soccerMatch.getMatch(), memoryEventStore);

        // Start
        matchHandler.handle(new StartMatchCommand(matchId));

        return memoryEventStore.loadEventStream(matchId);
    }

    @Test
    public void restoringStateTest() {

        MemoryEventStream resultA = testMatchA();
        MemoryEventStream resultB = testMatchB();

        //Create new Match
        {
            SoccerMatchModel soccerMatch = SoccerMatchBuilder.build();
            MemoryEventStore memoryEventStore = new MemoryEventStore();
            MatchHandler matchHandler = new MatchHandler(soccerMatch.getMatch(), memoryEventStore);

            //Restore MatchA
            matchHandler.restoreState(resultA);
            MatchState state = matchHandler.getMatch().getState();
            assertThat("MatchA should be correctly restored.", state, equalTo(MatchState.ended));

            // Check if assigned cards are restored
            boolean frankHasCard = false, frankHasRedCard = false, pietHasCard = false, henkHasCard = false;
            for (PlayerModel playerModel : matchHandler.getMatch().getAssignedYellowCards()) {
                if (playerModel.getPlayerName().equals("frank")) {
                    frankHasCard = true;
                    break;
                }
            }
            for (PlayerModel playerModel : matchHandler.getMatch().getAssignedRedCards()) {
                if (playerModel.getPlayerName().equals("piet")) {
                    pietHasCard = true;
                }
                if (playerModel.getPlayerName().equals("henk")) {
                    henkHasCard = true;
                }
                if (playerModel.getPlayerName().equals("frank")) {
                    frankHasRedCard = true;
                }
            }

            assertThat("MatchA should have assigned a yellow card to frank.", frankHasCard, equalTo(true));
            assertThat("MatchA should have assigned a red card to piet.", pietHasCard, equalTo(true));
            assertThat("MatchA should have assigned a red card to henk.", henkHasCard, equalTo(true));
            assertThat("MatchA should not have assigned a red card to frank.", frankHasRedCard, equalTo(false));
        }
        {
            //Create new Match
            SoccerMatchModel soccerMatchB = SoccerMatchBuilder.build();
            MemoryEventStore memoryEventStoreB = new MemoryEventStore();
            MatchHandler matchHandlerB = new MatchHandler(soccerMatchB.getMatch(), memoryEventStoreB);

            //Restore MatchB
            matchHandlerB.restoreState(resultB);
            MatchState stateB = matchHandlerB.getMatch().getState();
            boolean equals = stateB.equals(MatchState.started);
            assertThat("MatchB should be correctly restored.", equals, equalTo(true));
        }
    }
}
