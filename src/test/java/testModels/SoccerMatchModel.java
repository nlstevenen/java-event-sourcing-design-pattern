package testModels;

import match.aggregate.Match;
import match.player.PlayerModel;

import java.util.Set;

public class SoccerMatchModel {

    private Set<PlayerModel> playerSet;
    private Match match;

    public SoccerMatchModel(Set<PlayerModel> playerSet, Match match) {
        this.playerSet = playerSet;
        this.match = match;
    }

    public Set<PlayerModel> getPlayerSet() {
        return playerSet;
    }

    public Match getMatch() {
        return match;
    }

}
