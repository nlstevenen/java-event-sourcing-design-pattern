package testModels;

import match.aggregate.Match;
import match.player.PlayerModel;
import match.player.PlayerState;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class SoccerMatchBuilder {
    private SoccerMatchBuilder() {
    }

    public static SoccerMatchModel build() {
        Set<PlayerModel> players = new HashSet<PlayerModel>();
        UUID matchId = UUID.randomUUID();

        players.add(new PlayerModel(UUID.randomUUID(), "piet", PlayerState.playing));
        players.add(new PlayerModel(UUID.randomUUID(), "klaas", PlayerState.playing));
        players.add(new PlayerModel(UUID.randomUUID(), "frank", PlayerState.playing));
        players.add(new PlayerModel(UUID.randomUUID(), "henk", PlayerState.playing));

        Match match = new Match(matchId, players);

        return new SoccerMatchModel(players, match);
    }
}
