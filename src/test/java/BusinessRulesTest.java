import match.aggregate.Match;
import match.command.*;
import match.player.PlayerModel;
import match.player.PlayerState;
import org.junit.Before;
import org.junit.Test;
import testModels.SoccerMatchBuilder;
import testModels.SoccerMatchModel;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;


public class BusinessRulesTest {
    private UUID matchId;
    private Match match;

    @Before
    public void setUp() {
        SoccerMatchModel build = SoccerMatchBuilder.build();
        this.matchId = build.getMatch().getMatchId();
        this.match = build.getMatch();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotBeAbleToStartSameMatchTwice() {
        startMatch();
        startMatch();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotBeAbleToEndAnEndedMatch() {
        startMatch();
        endMatch();
        endMatch();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotBeAbleToEndANotStartedMatch() {
        endMatch();
        endMatch();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldNotBeAbleToStartAnEndedMatch() {
        startMatch();
        endMatch();
        startMatch();
    }

    @Test
    public void assigningRedCardShouldDisqualifyPlayer() {
        startMatch();
        PlayerModel playerModel = match.getPlayerSet().iterator().next();
        assignRedCard(playerModel);
        assertThat(playerModel.getPlayerState(), equalTo(PlayerState.disqualified));
    }

    @Test
    public void disqualifyingPlayerShouldResultInDisqualification() {
        startMatch();
        PlayerModel playerModel = match.getPlayerSet().iterator().next();
        match.handle(new DisqualifyPlayerCommand(matchId, playerModel));
        assertThat(playerModel.getPlayerState(), equalTo(PlayerState.disqualified));
    }

    @Test
    public void twoYellowCardsShouldResultInDisqualification() {
        PlayerModel playerModel = match.getPlayerSet().iterator().next();
        startMatch();
        assignYellowCard(playerModel);
        assignYellowCard(playerModel);
        assertThat(playerModel.getPlayerState(), equalTo(PlayerState.disqualified));
    }

    @Test
    public void oneYellowCardShouldNotResultInDisqualification() {
        PlayerModel playerModel = match.getPlayerSet().iterator().next();
        startMatch();
        assignYellowCard(playerModel);
        assertThat(playerModel.getPlayerState(), equalTo(PlayerState.playing));
    }

    private void assignYellowCard(PlayerModel playerModel) {
        match.handle(new AssignYellowCardCommand(matchId, playerModel));
    }

    private void startMatch() {
        match.handle(new StartMatchCommand(matchId));
    }

    private void endMatch() {
        match.handle(new EndMatchCommand(matchId));
    }

    private void assignRedCard(PlayerModel playerModel) {
        match.handle(new AssignRedCardCommand(matchId, playerModel));
    }
}
