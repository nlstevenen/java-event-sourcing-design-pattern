package match.store;

import match.api.Event;
import match.api.EventStore;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class MemoryEventStore implements EventStore {

    private volatile Map<UUID, MemoryEventStream> streams = new ConcurrentHashMap<UUID, MemoryEventStream>();

    @Override
    public MemoryEventStream loadEventStream(UUID aggregateId) {
        MemoryEventStream memoryEventStream = streams.get(aggregateId);

        if (memoryEventStream == null) {
            memoryEventStream = new MemoryEventStream();
            streams.put(aggregateId, memoryEventStream);
        }
        return memoryEventStream;
    }

    @Override
    public void store(UUID aggregateId, Event event) {
        MemoryEventStream memoryEventStream = loadEventStream(aggregateId);

        streams.put(aggregateId, memoryEventStream.append(event));
    }

    @Override
    public void store(UUID aggregateId, List<Event> events) {
        MemoryEventStream memoryEventStream = loadEventStream(aggregateId);

        streams.put(aggregateId, memoryEventStream.append(events));
    }
}
