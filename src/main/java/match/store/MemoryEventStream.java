package match.store;

import match.api.Event;
import match.api.EventStream;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Has an immutable <Code>List</Code> which can be appended.
 */
public class MemoryEventStream implements EventStream {
    private final List<Event> eventList;

    public MemoryEventStream() {
        this.eventList = Collections.emptyList();
    }

    public MemoryEventStream(List<Event> eventList) {
        this.eventList = eventList;
    }

    @Override
    public Iterator<Event> iterator() {
        return eventList.iterator();
    }

    public MemoryEventStream append(Event event) {

        List<Event> eventList = new LinkedList<Event>(this.eventList);
        eventList.add(event);
        return new MemoryEventStream(Collections.unmodifiableList(eventList));
    }

    public MemoryEventStream append(List<Event> events) {

        List<Event> eventList = new LinkedList<Event>(this.eventList);
        eventList.addAll(events);
        return new MemoryEventStream(Collections.unmodifiableList(eventList));
    }
}
