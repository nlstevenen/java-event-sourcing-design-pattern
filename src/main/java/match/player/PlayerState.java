package match.player;

public enum PlayerState {
    playing,
    injured,
    disqualified,
    substitute
}
