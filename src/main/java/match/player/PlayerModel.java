package match.player;

import java.util.UUID;

public class PlayerModel {

    private final UUID playerId;
    private final String playerName;
    private PlayerState playerState;

    public PlayerModel(UUID playerId, String playerName, PlayerState playerState) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerState = playerState;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }
}
