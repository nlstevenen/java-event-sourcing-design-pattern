package match.command;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Command;
import match.api.Event;
import match.event.EndMatchEvent;

import java.util.UUID;

public class EndMatchCommand implements Command {

    private final UUID matchId;

    public EndMatchCommand(UUID matchId) {
        this.matchId = matchId;
    }

    @Override
    public UUID getAggregateId() {
        return this.matchId;
    }

    @Override
    public Event execute(Match match) {

        MatchState state = match.getState();
        if (state != MatchState.started)
            throw new IllegalStateException(state.toString());
        return new EndMatchEvent();
    }
}

