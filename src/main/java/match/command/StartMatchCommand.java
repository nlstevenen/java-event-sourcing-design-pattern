package match.command;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Command;
import match.api.Event;
import match.event.StartMatchEvent;

import java.util.UUID;


public class StartMatchCommand implements Command {

    private final UUID matchId;

    public StartMatchCommand(UUID matchId) {
        this.matchId = matchId;
    }

    @Override
    public UUID getAggregateId() {
        return matchId;
    }

    @Override
    public Event execute(Match match) {
        MatchState state = match.getState();
        if (state != MatchState.notStarted)
            throw new IllegalStateException(state.toString());
        return new StartMatchEvent();
    }
}
