package match.command;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Command;
import match.api.Event;
import match.event.AssignYellowCardEvent;
import match.player.PlayerModel;

import java.util.Optional;
import java.util.UUID;

public class AssignYellowCardCommand implements Command {

    private final UUID matchId;
    private final PlayerModel playerModel;

    public AssignYellowCardCommand(UUID matchId, PlayerModel playerID) {
        this.matchId = matchId;
        this.playerModel = playerID;
    }

    @Override
    public UUID getAggregateId() {
        return this.matchId;
    }

    public PlayerModel getPlayerModel() {
        return playerModel;
    }

    @Override
    public Event execute(Match match) {
        MatchState state = match.getState();
        if (state != MatchState.started)
            throw new IllegalStateException(state.toString());

        AssignYellowCardEvent assignYellowCardEvent = new AssignYellowCardEvent(this.getPlayerModel());

        if (hasYellowCard(match)) {
            Event redCardEvent = assignRedCard(match);
            assignYellowCardEvent.setSucceedingEvent(redCardEvent);
            match.addAssignedRedCard(playerModel);
        }
        match.addAssignedYellowCard(playerModel);

        return assignYellowCardEvent;
    }

    private Event assignRedCard(Match match) {
        AssignRedCardCommand redCardCommand = new AssignRedCardCommand(this.getAggregateId(), this.getPlayerModel());
        return match.handle(redCardCommand);
    }

    private boolean hasYellowCard(Match match) {
        Optional<PlayerModel> modelOptional = match.getAssignedYellowCards().parallelStream()
                .filter(playerModel -> playerModel.getPlayerId().equals(getPlayerModel().getPlayerId()))
                .findAny();
        return modelOptional.isPresent() ? true : false;
    }
}
