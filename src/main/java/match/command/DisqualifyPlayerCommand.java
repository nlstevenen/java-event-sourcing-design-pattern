package match.command;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Command;
import match.api.Event;
import match.event.DisqualifyPlayerEvent;
import match.player.PlayerModel;

import java.util.UUID;

public class DisqualifyPlayerCommand implements Command {

    private final UUID matchId;
    private final PlayerModel playerModel;

    public DisqualifyPlayerCommand(UUID matchId, PlayerModel playerModel) {
        this.matchId = matchId;
        this.playerModel = playerModel;
    }

    public UUID getMatchId() {
        return matchId;
    }

    public PlayerModel getPlayerModel() {
        return playerModel;
    }

    @Override
    public UUID getAggregateId() {
        return this.matchId;
    }

    @Override
    public Event execute(Match match) {
        MatchState state = match.getState();
        if (state == MatchState.ended || state == MatchState.notStarted)
            throw new IllegalStateException(state.toString());
        return new DisqualifyPlayerEvent(this.getPlayerModel(), this.getMatchId());
    }
}
