package match.command;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Command;
import match.api.Event;
import match.event.AssignRedCardEvent;
import match.player.PlayerModel;

import java.util.UUID;

public class AssignRedCardCommand implements Command {

    private final UUID matchId;
    private final PlayerModel playerId;

    public AssignRedCardCommand(UUID matchId, PlayerModel playerID) {
        this.matchId = matchId;
        this.playerId = playerID;
    }

    @Override
    public UUID getAggregateId() {
        return this.matchId;
    }

    public PlayerModel getPlayerId() {
        return playerId;
    }

    @Override
    public Event execute(Match match) {
        MatchState state = match.getState();
        if (state != MatchState.started)
            throw new IllegalStateException(state.toString());
        return new AssignRedCardEvent(this.getPlayerId());
    }
}
