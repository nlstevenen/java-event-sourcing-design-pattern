package match.aggregate;

import match.api.Command;
import match.api.Event;
import match.player.PlayerModel;
import match.player.PlayerState;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Match {

    private final UUID matchId;
    private final Set<PlayerModel> playerSet;
    private final Set<PlayerModel> assignedYellowCards;
    private final Set<PlayerModel> assignedRedCards;
    private MatchState state = MatchState.notStarted;

    public Match(UUID matchId, Set<PlayerModel> playerSet) {
        this.matchId = matchId;
        this.playerSet = playerSet;
        assignedYellowCards = new HashSet<PlayerModel>();
        assignedRedCards = new HashSet<PlayerModel>();
    }

    public Set<PlayerModel> getAssignedYellowCards() {
        return assignedYellowCards;
    }

    public Set<PlayerModel> getAssignedRedCards() {
        return assignedRedCards;
    }

    public void addAssignedYellowCard(PlayerModel playerModel) {
        this.assignedYellowCards.add(playerModel);
    }

    public void addAssignedRedCard(PlayerModel playerModel) {
        this.assignedRedCards.add(playerModel);
    }

    public Event handle(Command command) {
        Event event = command.execute(this);
        return handle(event);
    }

    public Event handle(Event event) {
        return event.execute(this);
    }

    public UUID getMatchId() {
        return matchId;
    }

    public Set<PlayerModel> getPlayerSet() {
        return playerSet;
    }

    public MatchState getState() {
        return state;
    }

    public void setState(MatchState state) {
        this.state = state;
    }

    public void disqualifyPlayer(PlayerModel playerModel) {
        for (PlayerModel model : this.playerSet) {
            if (model.equals(playerModel)) {
                model.setPlayerState(PlayerState.disqualified);
            }
        }
    }
}
