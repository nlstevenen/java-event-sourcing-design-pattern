package match.aggregate;

public enum MatchState {
    notStarted,
    started,
    ended
}
