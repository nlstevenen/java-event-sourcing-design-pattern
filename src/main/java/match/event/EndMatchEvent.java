package match.event;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Event;


public class EndMatchEvent extends AbstractEvent {

    @Override
    public Event execute(Match match) {
        match.setState(MatchState.ended);
        return this;
    }
}
