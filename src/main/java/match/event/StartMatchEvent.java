package match.event;

import match.aggregate.Match;
import match.aggregate.MatchState;
import match.api.Event;


public class StartMatchEvent extends AbstractEvent {

    @Override
    public Event execute(Match match) {
        match.setState(MatchState.started);
        return this;
    }
}
