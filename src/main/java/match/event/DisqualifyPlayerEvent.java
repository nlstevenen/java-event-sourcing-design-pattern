package match.event;

import match.aggregate.Match;
import match.api.Event;
import match.player.PlayerModel;
import match.player.PlayerState;

import java.util.Optional;
import java.util.UUID;

public class DisqualifyPlayerEvent extends AbstractEvent {

    private final UUID matchId;
    private final PlayerModel playerModel;

    public DisqualifyPlayerEvent(PlayerModel playerModel, UUID matchId) {
        this.playerModel = playerModel;
        this.matchId = matchId;
    }

    public UUID getMatchId() {
        return matchId;
    }

    public PlayerModel getPlayerModel() {
        return playerModel;
    }

    @Override
    public Event execute(Match match) {

        for (PlayerModel playerModel : match.getPlayerSet()) {
            if (playerModel.getPlayerId().equals(this.getPlayerModel().getPlayerId())) {
                playerModel.setPlayerState(PlayerState.disqualified);
                break;
            }
        }
        return this;
    }

    @Override
    public Optional<Event> getSucceedingEvent() {
        return null;
    }

    @Override
    public void setSucceedingEvent(Event event) {

    }
}


