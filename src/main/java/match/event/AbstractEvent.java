package match.event;

import match.api.Event;

import java.util.Optional;

/**
 * Created by Steven Kok on 04/07/2014
 */
public abstract class AbstractEvent implements Event {

    private Optional<Event> succeedingEvent = Optional.empty();

    @Override
    public Optional<Event> getSucceedingEvent() {
        return this.succeedingEvent;
    }

    @Override
    public void setSucceedingEvent(Event event) {
        succeedingEvent = Optional.of(event);
    }
}
