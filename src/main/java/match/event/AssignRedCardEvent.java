package match.event;

import match.aggregate.Match;
import match.api.Event;
import match.player.PlayerModel;


public class AssignRedCardEvent extends AbstractEvent {

    private final PlayerModel playerModel;

    public AssignRedCardEvent(PlayerModel playerModel) {
        this.playerModel = playerModel;
    }

    @Override
    public Event execute(Match match) {
        match.addAssignedRedCard(playerModel);
        match.disqualifyPlayer(playerModel);
        return this;
    }
}
