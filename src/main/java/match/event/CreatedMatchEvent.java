package match.event;

import match.aggregate.Match;
import match.api.Event;
import match.player.PlayerModel;

import java.util.Set;
import java.util.UUID;


public class CreatedMatchEvent extends AbstractEvent {

    private final UUID matchId;
    private final Set<PlayerModel> playersMap;

    public CreatedMatchEvent(UUID matchId, Set<PlayerModel> playersMap) {
        this.matchId = matchId;
        this.playersMap = playersMap;
    }

    public UUID getMatchId() {
        return matchId;
    }

    public Set<PlayerModel> getPlayersMap() {
        return playersMap;
    }

    @Override
    public Event execute(Match match) {
        return this;
    }
}
