package match.event;

import match.aggregate.Match;
import match.api.Event;
import match.player.PlayerModel;


public class AssignYellowCardEvent extends AbstractEvent {

    private final PlayerModel playerModel;

    public AssignYellowCardEvent(PlayerModel playerModel) {
        this.playerModel = playerModel;
    }

    @Override
    public Event execute(Match match) {
        match.addAssignedYellowCard(this.playerModel);
        return this;
    }
}
