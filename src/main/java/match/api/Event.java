package match.api;

import match.aggregate.Match;

import java.util.Optional;

public interface Event {
    public Event execute(Match match);

    public Optional<Event> getSucceedingEvent();

    public void setSucceedingEvent(Event event);

}
