package match.api;

import match.aggregate.Match;

import java.util.UUID;

public interface Command {
    UUID getAggregateId();

    Event execute(Match match);
}
