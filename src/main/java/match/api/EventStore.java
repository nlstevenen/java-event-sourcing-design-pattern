package match.api;

import java.util.List;
import java.util.UUID;

public interface EventStore {

    EventStream loadEventStream(UUID aggregateId);

    void store(UUID aggregateId, Event event);

    void store(UUID aggregateId, List<Event> events);
}
